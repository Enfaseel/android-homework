package ru.skillbranch.calc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //получаем ссылку на textView(куда выводим ответ)
        val answer:TextView = findViewById(R.id.answer)

        //ссылки на EditText - поля для ввода текста
        val fistNumber:EditText = findViewById(R.id.number1)
        val secondNumber:EditText = findViewById(R.id.number2)

        //ссылка на кнпоку
        val plus = findViewById<Button>(R.id.plus)

        //устанавливаем слушатель нажатия на кнопку
        plus.setOnClickListener{
            val a = fistNumber.text.toString()
            val b = secondNumber.text.toString()
            answer.text = (a.toInt() + b.toInt()).toString()
        }
    }
}
