package `5`

fun main(args: Array<String>) {
    var originalMap: Map<String, Int> = mutableMapOf("Mike" to 1000000, "Murad" to 299, "Narek" to 301, "ABRACODABRA" to 200)
    println(originalMap)
    originalMap = originalMap.filter { it.value > 300 }
    println(originalMap)
}