﻿// ЗАДАНИЕ 1. Шухер рыжая

fun main() {
    val a = "Shukher ryzhaya!"; 
    println(a);
    val b = a.reversed(); //var.REVERSED() меняет порядок
    println(b);
}

// ЗАДАНИЕ 2. Бухло на тазах

fun main() {
    val buhlo: Int = 14; //Блять, как делать ебаный ввод с клавы, сука??? readline() сосёт!
    println("Skoko vypil? ");
    println(buhlo);
    
  
    //цикл с when
    when (buhlo){
        in 1..4 -> println("Za Moskovskii blablablablabla"); 
        in 5..8 -> println("Za MGTU");
        in 9..12 -> println("Ya engineer!");
        in 13..16 -> println("Ya bergmerpgic!");
    }
    
    //цикл с if
    if (buhlo in 1..4){ 
        println("Za Moskovskii blablablablabla");
    }
    if (buhlo in 5..8){ 
        println("Za MGTU");
    }
    if (buhlo in 9..12){ 
        println("Ya engineer");
    }
    if (buhlo in 13..16){ 
        println("Ya negeoegebb");
    }
}

// ЗАДАНИЕ 3. Функции 

fun main() {
    val buhlo: Int = 15;
    println("Skoko vypil? ");
    println(buhlo);
    
    //цикл с if
    if (buhlo in 1..4){ 
        vivod1();
    }
    if (buhlo in 5..8){ 
        vivod2();
    }
    if (buhlo in 9..12){ 
        vivod3();
    }
    if (buhlo in 13..16){ 
        vivod4();
    }
}

fun vivod1(){
    println("ZA Ordena Lenina!");
} 

fun vivod2(){
    println("ZA MGTU!");
} 

fun vivod3(){
    println("Ya engineer");
} 

fun vivod4(){
    println("Ya rucecgvtvht");
} 

// ЗАДАНИЕ 4. Массив строк 

fun main() {
    val list:List<String> = listOf("govno", "zalupa", "penis", "her", "davalka",  "hui", "blyadina")
    println(list)
    println(list.joinToString(prefix = "[", postfix = "]", separator = ""))
}

// ЗАДАНИЕ 5. Map и деньги

fun main(){
    var students: MutableMap <String, Int> = mutableMapOf("Ivan" to 300, "Dmitry" to 500, "Nastya" to 100, "Masha" to 1000, "Vasya" to 0, "Alex" to 299, "Roman" to 16000)
    println("Bablo studentov:" + students)
    //пишем итератор
    val iterator = students.entries.iterator() //объявление итератора
    while (iterator.hasNext()){ //проход по мапе с помощью итератора
        val pair_stud_KV = iterator.next() //переменная для пары ключ-значение
        if (pair_stud_KV.value < 300){ //сравнение суммы
            iterator.remove()    //кик нищука
        }
    }
    println("Bablo studentov(bez bomjei):" + students)
}

